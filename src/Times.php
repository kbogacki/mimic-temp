<?php
namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\TimesVerifiers\AtLeast;
use Gstarczyk\Mimic\TimesVerifiers\Exactly;

class Times
{
    /**
     * @param int $times
     * @return TimesVerifier
     */
    static public function atLeast($times)
    {
        return new AtLeast($times);
    }

    /**
     * @param int $times
     * @return TimesVerifier
     */
    static public function exactly($times)
    {
        return new Exactly($times);
    }

    /**
     * @return TimesVerifier
     */
    static public function once()
    {
        return new Exactly(1);
    }

    /**
     * @return TimesVerifier
     */
    static public function never()
    {
        return new Exactly(0);
    }
}
