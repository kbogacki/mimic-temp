<?php
namespace Gstarczyk\Mimic\IntegrationTest\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsCaptor;
use Gstarczyk\Mimic\IntegrationTest\Fixture\Mock1;
use Gstarczyk\Mimic\Mimic;
use Gstarczyk\Mimic\Times;
use PHPUnit_Framework_Assert as Assert;

class ArgumentsCaptorTest extends \PHPUnit_Framework_TestCase
{
    public function testArgumentCaptor()
    {
        /** @var Mock1 $mock */
        $mock = Mimic::mock(Mock1::class);

        $mock->publicMethodOne('someClass');
        $mock->publicMethodOne('someOtherClass');
        $mock->publicMethodOne('someMoreOtherClass');

        $argumentsCaptor = new ArgumentsCaptor();
        Mimic::verify($mock)
            ->method('publicMethodOne')
            ->with($argumentsCaptor)
            ->wasCalled(Times::exactly(3));

        $result = $argumentsCaptor->getValues();

        Assert::assertEquals(['someClass'], $result[0]);
        Assert::assertEquals(['someOtherClass'], $result[1]);
        Assert::assertEquals(['someMoreOtherClass'], $result[2]);
    }
}
