<?php
namespace Gstarczyk\Mimic\MockInitiator;

class ContextFactory
{
    const NAMESPACE_SEPARATOR = '\\';

    /** @var ImportsExtractor */
    private $importsExtractor;

    public function __construct(ImportsExtractor $importsExtractor)
    {
        $this->importsExtractor = $importsExtractor;
    }

    /**
     * @param object $parentObject
     * @param string $pathToParentObjectFile
     * @return Context
     */
    public function createContext($parentObject, $pathToParentObjectFile)
    {
        if ($pathToParentObjectFile) {
            $imports = $this->importsExtractor->getImports($pathToParentObjectFile);
        } else {
            $imports = [];
        }
        $parts = explode(self::NAMESPACE_SEPARATOR, get_class($parentObject));
        array_pop($parts);
        $namespace = self::NAMESPACE_SEPARATOR . implode(self::NAMESPACE_SEPARATOR, $parts);

        return new Context($parentObject, $namespace, $imports);
    }
}