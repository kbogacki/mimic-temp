<?php
namespace Gstarczyk\Mimic\TimesVerifiers;

use Gstarczyk\Mimic\TimesVerifier;

abstract class AbstractVerifier implements TimesVerifier
{
    /** @var int */
    private $expectedTimes = 0;

    /**
     * @param int $expectedTimes
     */
    public function __construct($expectedTimes)
    {
        $this->expectedTimes = $expectedTimes;
    }

    /**
     * @return int
     */
    protected function getExpectedTimes()
    {
        return $this->expectedTimes;
    }
}