<?php
namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class StringEndsWith implements ValueMatcher
{
    private $suffix;

    /**
     * @param mixed $suffix
     */
    public function __construct($suffix)
    {
        $this->suffix = $suffix;
    }

    public function match($value)
    {
        $suffixLength = mb_strlen($this->suffix);
        return mb_substr($value, -$suffixLength) == $this->suffix;
    }
}