<?php
namespace Gstarczyk\Mimic\UnitTest\Fixture\InitMocks;

class TestClassOne
{
    private $arg1;

    /**
     * @var \stdClass
     */
    private $arg2;

    public function __construct($arg1, \stdClass $arg2)
    {
        $this->arg1 = $arg1;
        $this->arg2 = $arg2;
    }

    public function publicMethodOne($arg1)
    {
        return 100;
    }

    public function publicMethodTwo(\DateTime $arg1, array $arg2 = [])
    {
        return 100;
    }

    public function publicMethodThree(TestClassOne $arg1, \DateTime $arg2 = null)
    {
        return 100;
    }

    public function publicMethodFour($arg1 = 'string', $arg2 = 100)
    {
        return 100;
    }

    protected function protectedMethodOne($arg1)
    {
    }

    private function privateMethodOne($arg1)
    {
    }
}
