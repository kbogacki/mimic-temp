<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\FileReader;
use Gstarczyk\Mimic\MockInitiator\ImportFactory;
use Gstarczyk\Mimic\MockInitiator\ImportsExtractor;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class ImportsExtractorTest extends \PHPUnit_Framework_TestCase
{
    /** @var ImportsExtractor */
    private $extractor;

    /** @var ImportFactory | MockObject */
    private $importFactory;

    /** @var FileReader | MockObject */
    private $fileReader;

    protected function setUp()
    {
        $this->importFactory = $this->createMock(ImportFactory::class);
        $this->fileReader = $this->createMock(FileReader::class);
        $this->extractor = new ImportsExtractor(
            $this->importFactory,
            $this->fileReader
        );
    }

    /**
     * @param string $fileContent
     * @dataProvider simpleImportsProvider
     */
    public function testExtractSimpleImports($fileContent)
    {
        $this->importFactory->expects($this->exactly(2))
            ->method('createImport')
            ->withConsecutive(
                ['My\Name\Space\MyClassOne'],
                ['My\Name\Space\MyClassTwo']
            );
        $this->fileReader->expects($this->any())
            ->method('getContents')
            ->willReturn($fileContent);

        $result = $this->extractor->getImports('\path\to\file.ext');

        Assert::assertTrue(is_array($result));
        Assert::assertCount(2, $result);
    }

    public function simpleImportsProvider()
    {
        return [
            'multi statement imports' => ['
                namespace My\Other\Name\Space;
                
                use My\Name\Space\MyClassOne;
                use My\Name\Space\MyClassTwo;
               
                class MyClass {}
             '
            ],
            'single statement imports in single line'=>['
                namespace My\Other\Name\Space;
            
                use My\Name\Space\MyClassOne, My\Name\Space\MyClassTwo;
               
                class MyClass {}
             '
            ],
            'single statement imports in multi lines'=>['
                namespace My\Other\Name\Space;
            
                use My\Name\Space\MyClassOne, 
                    My\Name\Space\MyClassTwo;
               
                class MyClass {}
             '
            ]
        ];
    }

    /**
     * @param string $fileContent
     * @dataProvider aliasedImportsProvider
     */
    public function testExtractAliasedImports($fileContent)
    {
        $this->importFactory->expects($this->exactly(2))
            ->method('createImport')
            ->withConsecutive(
                ['My\Name\Space\MyClassOne', 'OtherClassOne'],
                ['My\Name\Space\MyClassTwo', 'OtherClassTwo']
            );

        $this->fileReader->expects($this->any())
            ->method('getContents')
            ->willReturn($fileContent);

        $result = $this->extractor->getImports('\path\to\file.ext');

        Assert::assertTrue(is_array($result));
        Assert::assertCount(2, $result);
    }

    public function aliasedImportsProvider()
    {
        return [
            'multi statements imports' => ['
                namespace My\Other\Name\Space;
                
                use My\Name\Space\MyClassOne as OtherClassOne;
                use My\Name\Space\MyClassTwo as OtherClassTwo;
               
                class MyClass {}
             '
            ],
            'single statement imports in single line'=>['
                namespace My\Other\Name\Space;
            
                use My\Name\Space\MyClassOne as OtherClassOne, My\Name\Space\MyClassTwo as OtherClassTwo;
               
                class MyClass {}
             '
            ],
            'single statement imports in multi lines'=>['
                namespace My\Other\Name\Space;
            
                use My\Name\Space\MyClassOne as OtherClassOne, 
                    My\Name\Space\MyClassTwo as OtherClassTwo
                ;
               
                class MyClass {}
             '
            ]
        ];
    }

}
