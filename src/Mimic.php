<?php
namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\Mock\ClassDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use Gstarczyk\Mimic\Mock\MockFactory;
use Gstarczyk\Mimic\MockInitiator\ContextFactory;
use Gstarczyk\Mimic\MockInitiator\FileReader;
use Gstarczyk\Mimic\MockInitiator\ImportFactory;
use Gstarczyk\Mimic\MockInitiator\ImportsExtractor;
use Gstarczyk\Mimic\MockInitiator\MethodArgumentsResolver;
use Gstarczyk\Mimic\MockInitiator\MockInitiator;
use Gstarczyk\Mimic\MockInitiator\ObjectPropertyFactory;
use Gstarczyk\Mimic\MockInitiator\PropertyExtractor;
use Gstarczyk\Mimic\MockInitiator\TargetObjectFactory;
use Gstarczyk\Mimic\MockInitiator\TypeResolver;
use Gstarczyk\Mimic\Stub\BehaviourBuilder\BehaviourBuilderSelector;
use Gstarczyk\Mimic\VerifierBuilder\VerifierBuilderSelector;

class Mimic
{
    static private $registry;

    /**
     * @return MimicRegistry
     */
    static private function getRegistry()
    {
        if (!isset(self::$registry)) {
            $mockFactory = new MockFactory(
                new ClassDefinitionFactory(new UniqueIdGenerator(), new MethodArgumentFactory())
            );
            self::$registry = new MimicRegistry($mockFactory);
        }

        return self::$registry;
    }

    /**
     * @param string $className
     * @return object
     */
    static public function mock($className)
    {
        return self::getRegistry()->getMock($className);
    }

    /**
     * @param object $mock
     * @return BehaviourBuilderSelector
     */
    static public function when($mock)
    {
        return new BehaviourBuilderSelector(self::getRegistry()->getStub($mock));
    }

    /**
     * @param object $mock
     * @return VerifierBuilderSelector
     */
    static public function verify($mock)
    {
        return new VerifierBuilderSelector(self::getRegistry()->getInvocationRegistry($mock), new ArgumentsMatcherFactory());
    }

    /**
     * Automatically create mocks for annotated test case properties
     * @param object $testCase
     * @param string | null $pathToTestCaseFile
     */
    static public function initMocks($testCase, $pathToTestCaseFile = null)
    {
        $initiator = new MockInitiator(
            self::getRegistry(),
            new PropertyExtractor(
                new ObjectPropertyFactory(
                    new TypeResolver()
                ),
                new ContextFactory(
                    new ImportsExtractor(
                        new ImportFactory(),
                        new FileReader()
                    ))
            ),
            new TargetObjectFactory(
                new MethodArgumentsResolver()
            )
        );

        $initiator->initMocks($testCase, $pathToTestCaseFile);
    }
}