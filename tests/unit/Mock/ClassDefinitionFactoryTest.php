<?php
namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\InvocationHandler;
use Gstarczyk\Mimic\Mock\ClassAttributeDefinition;
use Gstarczyk\Mimic\Mock\ClassDefinition;
use Gstarczyk\Mimic\Mock\ClassDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use Gstarczyk\Mimic\UniqueIdGenerator;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassOne;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassWithFinalMethod;
use Gstarczyk\Mimic\UnitTest\Fixture\FinalClass;
use Gstarczyk\Mimic\UnitTest\Fixture\StaticMethods;
use Gstarczyk\Mimic\UnitTest\Fixture\TestInterface;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use RuntimeException;

class ClassDefinitionFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var ClassDefinitionFactory */
    private $factory;

    /** @var  UniqueIdGenerator | MockObject */
    private $uniqueIdGeneratorMock;

    /** @var MethodArgumentFactory | MockObject */
    private $methodArgumentFactory;

    protected function setUp()
    {
        $this->uniqueIdGeneratorMock = $this->createMock(UniqueIdGenerator::class);
        $this->methodArgumentFactory = $this->createMock(MethodArgumentFactory::class);
        $this->factory = new ClassDefinitionFactory(
            $this->uniqueIdGeneratorMock,
            $this->methodArgumentFactory
        );
    }

    public function testFactoryThrowExceptionWhenGivenClassHasFinalMethod()
    {
        $this->expectException(\InvalidArgumentException::class);

        $definition = $this->factory->createClassDefinition(ClassWithFinalMethod::class);
        Assert::assertInstanceOf(ClassDefinition::class, $definition);
    }

    public function testFactoryThrowExceptionWhenGivenFinalClass()
    {
        $this->expectException(\InvalidArgumentException::class);

        $definition = $this->factory->createClassDefinition(FinalClass::class);
        Assert::assertInstanceOf(ClassDefinition::class, $definition);
    }

    public function testFactoryThrowExceptionWhenGivenClassNameIsNotExistingClassOrInterface()
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->factory->createClassDefinition('NotExistingClass');
    }

    public function testFactoryCreatesClassThatExtendsMockedClass()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('UniqueSuffix');
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertEquals(ClassOne::class, $definition->getExtends());
    }

    public function testFactoryCreatesClassWithUniqueClassName()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('UniqueSuffix');
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertEquals('ClassOne_UniqueSuffix', $definition->getShortName());
    }

    public function testFactoryCreatesClassInSameNamespaceAsMockedClass()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('SomeSuffix');
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertEquals('Gstarczyk\Mimic\UnitTest\Fixture', $definition->getNamespace());
    }

    public function testFactoryCreateClassThatOverwriteOnlyPublicMethods()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertCount(5, $definition->getMethodDefinitions());
    }

    public function testFactoryAlwaysCreateNewClass()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturnOnConsecutiveCalls('Mock1', 'Mock2');
        $definition1 = $this->factory->createClassDefinition(ClassOne::class);
        $definition2 = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertNotEquals($definition1->getShortName(), $definition2->getShortName());
    }

    public function testFactoryCanMockInterfaces()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');
        $definition = $this->factory->createClassDefinition(TestInterface::class);

        $this->assertNull($definition->getExtends());
        $this->assertEquals(TestInterface::class, $definition->getImplements());

        $methods = $definition->getMethodDefinitions();
        Assert::assertCount(3, $methods);
        Assert::assertEquals('__construct', $methods[0]->getMethodName());
        Assert::assertEquals('someMethod', $methods[1]->getMethodName());
        Assert::assertEquals('someOtherMethod', $methods[2]->getMethodName());
    }

    public function testCreatedClassRequireInvocationHandler()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');

        $definition = $this->factory->createClassDefinition(TestInterface::class);

        $this->assertClassRequireInvocationHandler($definition);
    }

    public function testStaticMethodsDoNotBreakMocking()
    {
        $this->expectException(RuntimeException::class);
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');

        $definition = $this->factory->createClassDefinition(StaticMethods::class);

        $definition->getMethodDefinition('staticMethod');
    }

    private function assertClassRequireInvocationHandler(ClassDefinition $definition)
    {
        $attribute = new ClassAttributeDefinition('__invocationHandler');
        $argument = new ObjectArgumentDefinition('invocationHandler', InvocationHandler::class);
        $constructor = $definition->getMethodDefinition('__construct');

        Assert::assertNotNull($constructor);
        Assert::assertContains($attribute, $definition->getAttributes(), '', false, false);
        Assert::assertContains($argument, $constructor->getArguments(), '', false, false);
    }
}
