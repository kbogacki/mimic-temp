<?php
namespace Gstarczyk\Mimic\Stub\Actions;

use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Action;

class DoNothing implements Action
{
    public function perform(InvocationSignature $invocationSignature)
    {
        return null;
    }
}