<?php
namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\TimesVerifiers\TimesVerificationException;

interface TimesVerifier
{
    /**
     * @param int $times
     * @throws TimesVerificationException
     */
    public function verify($times);
}