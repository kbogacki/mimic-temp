<?php
namespace Gstarczyk\Mimic\MockInitiator;

use Gstarczyk\Mimic\MimicRegistry;
use ReflectionClass;
use RuntimeException;

class MockInitiator
{
    /** @var MimicRegistry */
    private $mimicRegistry;

    /** @var TargetObjectFactory */
    private $targetObjectFactory;

    /** @var PropertyExtractor */
    private $propertyExtractor;

    public function __construct(
        MimicRegistry $mimicRegistry,
        PropertyExtractor $propertyExtractor,
        TargetObjectFactory $targetObjectFactory
    )
    {
        $this->mimicRegistry = $mimicRegistry;
        $this->propertyExtractor = $propertyExtractor;
        $this->targetObjectFactory = $targetObjectFactory;
    }

    /**
     * Create mocks for annotated properties of given test case
     * @param object $testCase
     * @param string | null $pathToTestCaseFile
     */
    public function initMocks($testCase, $pathToTestCaseFile = null)
    {
        $properties = $this->propertyExtractor->extractProperties($testCase, $pathToTestCaseFile);
        $mocks = $this->createMocks($properties);
        $this->injectMocks($properties, $mocks);
    }

    /**
     * @param ObjectProperty[] $properties
     * @return ObjectProperty[]
     */
    private function createMocks(array $properties)
    {
        $mockCandidates = array_filter($properties, function (ObjectProperty $property) {
            return $property->isMarkedAsMock();
        });
        foreach ($mockCandidates as $mockCandidate) {
            $mock = $this->createMock($mockCandidate);
            $mockCandidate->setValue($mock);
        }

        return $mockCandidates;
    }

    private function createMock(ObjectProperty $property)
    {
        $this->validateProperty($property);

        return $this->mimicRegistry->getMock($property->getType());
    }

    /**
     * @param ObjectProperty[] $properties
     * @param ObjectProperty[] $mocks
     */
    private function injectMocks(array $properties, array $mocks)
    {
        $targetCandidates = array_filter($properties, function (ObjectProperty $property) {
            return $property->isMarkedAsMocksTarget();
        });
        foreach ($targetCandidates as $targetCandidate) {
            $target = $this->createTarget($targetCandidate, $mocks);
            $targetCandidate->setValue($target);
        }
    }

    /**
     * @param ObjectProperty $property
     * @param ObjectProperty[] $dependencies
     * @return object
     */
    private function createTarget(ObjectProperty $property, array $dependencies)
    {
        $this->validateProperty($property);
        $reflectionClass = new ReflectionClass($property->getType());
        $target = $this->targetObjectFactory->createTargetObject($reflectionClass, $dependencies);

        return $target;
    }

    /**
     * @param ObjectProperty $property
     * @throws RuntimeException
     */
    private function validateProperty(ObjectProperty $property)
    {
        $type = $property->getType();
        if (!$type) {
            throw new RuntimeException('Property type is missing!');
        }
    }
}
