<?php
namespace Gstarczyk\Mimic\Stub\BehaviourBuilder\SingleInvocation;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\ArgumentsMatchers\AnyArguments;
use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\ArgumentsMatchers\EmptyArguments;
use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\Stub\Actions\ReturnCallbackResult;
use Gstarczyk\Mimic\Stub\Actions\ReturnValue;
use Gstarczyk\Mimic\Stub\Actions\ThrowException;
use Gstarczyk\Mimic\Stub\Behaviour;
use Gstarczyk\Mimic\Stub\Stub;

class BehaviourBuilder implements ArgumentsChooser, ReactionChooser
{
    /** @var Stub */
    private $stub;

    /** @var string */
    private $methodName;

    /** @var ArgumentsMatcher */
    private $argumentsMatcher;

    /** @var ArgumentsMatcherFactory */
    private $matchingArgumentsFactory;

    public function __construct(Stub $stub, ArgumentsMatcherFactory $matchingArgumentsFactory)
    {
        $this->stub = $stub;
        $this->matchingArgumentsFactory = $matchingArgumentsFactory;
    }

    /**
     * @param string $methodName
     * @return $this
     */
    public function invoke($methodName)
    {
        $this->methodName = $methodName;

        return $this;
    }

    public function with($_)
    {
        $this->argumentsMatcher = $this->matchingArgumentsFactory->createMatcher(func_get_args());

        return $this;
    }

    public function withAnyArguments()
    {
        $this->argumentsMatcher = new AnyArguments();

        return $this;
    }

    public function withoutArguments()
    {
        $this->argumentsMatcher = new EmptyArguments();

        return $this;
    }

    /**
     * @param mixed $value
     * @return void
     */
    public function willReturn($value)
    {
        $this->stub->registerBehaviour(
            new Behaviour(
                new ReturnValue($value),
                $this->createInvocationMatcher()
            )
        );
    }

    public function willReturnCallbackResult(\Closure $callback)
    {
        $this->stub->registerBehaviour(
            new Behaviour(
                new ReturnCallbackResult($callback),
                $this->createInvocationMatcher()
            )
        );
    }

    public function willThrow(\Exception $exception)
    {
        $this->stub->registerBehaviour(
            new Behaviour(
                new ThrowException($exception),
                $this->createInvocationMatcher()
            )
        );
    }

    private function createInvocationMatcher()
    {
        return new InvocationMatcher(
            $this->methodName,
            $this->argumentsMatcher
        );
    }
}