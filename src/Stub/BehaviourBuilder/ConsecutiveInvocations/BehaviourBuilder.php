<?php
namespace Gstarczyk\Mimic\Stub\BehaviourBuilder\ConsecutiveInvocations;

use Gstarczyk\Mimic\ArgumentsMatchers\AnyArguments;
use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\Stub\Actions\ReturnCallbackResult;
use Gstarczyk\Mimic\Stub\Actions\ReturnValue;
use Gstarczyk\Mimic\Stub\Actions\ThrowException;
use Gstarczyk\Mimic\Stub\Behaviour;
use Gstarczyk\Mimic\Stub\Stub;

class BehaviourBuilder implements ReactionChooser, NextReactionChooser
{
    /** @var Stub */
    private $stub;

    /** @var string */
    private $methodName;

    /** @var int */
    private $invocationNo = 0;

    public function __construct(Stub $stub)
    {
        $this->stub = $stub;
    }

    /**
     * @param string $methodName
     */
    public function setMethodName($methodName)
    {
        $this->methodName = $methodName;
    }

    public function willReturn($value)
    {
        $this->stub->registerBehaviour(
            new Behaviour(
                new ReturnValue($value),
                $this->createInvocationMatcher(),
                $this->getNextInvocationNumber()
            )
        );

        return $this;
    }

    public function willReturnCallbackResult(\Closure $callback)
    {
        $this->stub->registerBehaviour(
            new Behaviour(
                new ReturnCallbackResult($callback),
                $this->createInvocationMatcher(),
                $this->getNextInvocationNumber()
            )
        );

        return $this;
    }

    public function willThrow(\Exception $exception)
    {
        $this->stub->registerBehaviour(
            new Behaviour(
                new ThrowException($exception),
                $this->createInvocationMatcher(),
                $this->getNextInvocationNumber()
            )
        );

        return $this;
    }

    public function thenReturn($value)
    {
        return $this->willReturn($value);
    }

    public function thenReturnCallbackResult(\Closure $callback)
    {
        return $this->willReturnCallbackResult($callback);
    }

    public function thenThrow(\Exception $exception)
    {
        return $this->willThrow($exception);
    }

    /**
     * @return InvocationMatcher
     */
    private function createInvocationMatcher()
    {
        return new InvocationMatcher(
            $this->methodName,
            new AnyArguments()
        );
    }

    /**
     * @return int
     */
    private function getNextInvocationNumber()
    {
        return ++$this->invocationNo;
    }
}
