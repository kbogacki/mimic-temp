namespace Gstarczyk\Mimic\UnitTest\Fixture;
class ClassOne_Mock extends \Gstarczyk\Mimic\UnitTest\Fixture\ClassOne
{
private $__invocationHandler;
public function __construct(\Gstarczyk\Mimic\InvocationHandler $invocationHandler)
{
$this->__invocationHandler = $invocationHandler;
}
public function publicMethodOne($arg1)
{
return $this->__invocationHandler->handleInvocation(__FUNCTION__, func_get_args());
}
public function publicMethodTwo(\DateTime $arg1, array $arg2 = [])
{
return $this->__invocationHandler->handleInvocation(__FUNCTION__, func_get_args());
}
public function publicMethodThree(\Gstarczyk\Mimic\UnitTest\Fixture\ClassOne $arg1, \DateTime $arg2 = null)
{
return $this->__invocationHandler->handleInvocation(__FUNCTION__, func_get_args());
}
public function publicMethodFour($arg1 = 'string', $arg2 = 100)
{
return $this->__invocationHandler->handleInvocation(__FUNCTION__, func_get_args());
}
}