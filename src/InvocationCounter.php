<?php
namespace Gstarczyk\Mimic;

interface InvocationCounter
{
    /**
     * @param InvocationMatcher $invocationMatcher
     * @return int
     */
    public function invocationCount(InvocationMatcher $invocationMatcher);
}