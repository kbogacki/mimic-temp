<?php
namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class AnyFloatMatcher implements ValueMatcher
{
    public function match($value)
    {
        return is_float($value);
    }
}