<?php
namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\Mock\ClassDefinition;

class ClassDefinitionTest extends \PHPUnit_Framework_TestCase
{
    public function testGetMethodDefinitionThrowExceptionIfDefinitionForGivenNameIsNotFound()
    {
        $this->expectException(\RuntimeException::class);

        $definition = new ClassDefinition();
        $definition->getMethodDefinition('someMethodName');
    }

    public function testCodeGenerationForSimpleClass()
    {
        $definition = new ClassDefinition();
        $definition->setShortName('MyNewClass');
        $definition->setNamespace('MyNamespace');

        $expected = 'namespace MyNamespace;'.PHP_EOL
            .'class MyNewClass'.PHP_EOL
            .'{'.PHP_EOL
            .'}';

        $result = $definition->toCode();

        $this->assertEquals($expected, $result);
    }

    public function testCodeGenerationForClassExtendingOtherClass()
    {
        $definition = new ClassDefinition();
        $definition->setShortName('MyNewClass');
        $definition->setNamespace('MyNamespace');
        $definition->setExtends('MyOtherClass');

        $expected = 'namespace MyNamespace;'.PHP_EOL
            .'class MyNewClass extends \MyOtherClass'.PHP_EOL
            .'{'.PHP_EOL
            .'}';

        $result = $definition->toCode();

        $this->assertEquals($expected, $result);
    }

    public function testCodeGenerationForClassImplementsInterface()
    {
        $definition = new ClassDefinition();
        $definition->setShortName('MyNewClass');
        $definition->setNamespace('MyNamespace');
        $definition->setImplements('MyInterface');

        $expected = 'namespace MyNamespace;'.PHP_EOL
            .'class MyNewClass implements \MyInterface'.PHP_EOL
            .'{'.PHP_EOL
            .'}';

        $result = $definition->toCode();

        $this->assertEquals($expected, $result);
    }
}
