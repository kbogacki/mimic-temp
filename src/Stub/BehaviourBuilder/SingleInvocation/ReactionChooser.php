<?php
namespace Gstarczyk\Mimic\Stub\BehaviourBuilder\SingleInvocation;

interface ReactionChooser
{
    /**
     * @param mixed $value
     * @return void
     */
    public function willReturn($value);

    /**
     * @param \Closure $callback
     * @return void
     */
    public function willReturnCallbackResult(\Closure $callback);

    /**
     * @param \Exception $exception
     * @return void
     */
    public function willThrow(\Exception $exception);
}