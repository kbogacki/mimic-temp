<?php
namespace Gstarczyk\Mimic\UnitTest\Stub\BehaviourBuilder\ConsecutiveInvocations;

use Gstarczyk\Mimic\ArgumentsMatchers\AnyArguments;
use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\Stub\Actions\ReturnCallbackResult;
use Gstarczyk\Mimic\Stub\Actions\ReturnValue;
use Gstarczyk\Mimic\Stub\Actions\ThrowException;
use Gstarczyk\Mimic\Stub\Behaviour;
use Gstarczyk\Mimic\Stub\BehaviourBuilder\ConsecutiveInvocations\BehaviourBuilder;
use Gstarczyk\Mimic\Stub\Stub;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class BehaviourBuilderTest extends \PHPUnit_Framework_TestCase
{
    /** @var BehaviourBuilder */
    private $builder;

    /** @var Stub | MockObject */
    private $stub;

    protected function setUp()
    {
        $this->stub = $this->createMock(Stub::class);
        $this->builder = new BehaviourBuilder($this->stub);
    }

    public function testBuilderRegisterBehaviourForAllDefinedActions()
    {
        $expectedBehaviours = [
            new Behaviour(
                new ReturnValue('first'),
                new InvocationMatcher('someMethod', new AnyArguments()),
                1
            ),
            new Behaviour(
                new ReturnCallbackResult(function (){}),
                new InvocationMatcher('someMethod', new AnyArguments()),
                2
            ),
            new Behaviour(
                new ThrowException(new \RuntimeException('')),
                new InvocationMatcher('someMethod', new AnyArguments()),
                3
            )
        ];

        $this->stub->expects($this->exactly(3))
            ->method('registerBehaviour')
            ->withConsecutive(
                [$expectedBehaviours[0]],
                [$expectedBehaviours[1]],
                [$expectedBehaviours[2]]
            );

        $this->builder->setMethodName('someMethod');
        $this->builder->willReturn('first');
        $this->builder->thenReturnCallbackResult(function (){});
        $this->builder->thenThrow(new \RuntimeException(''));
    }
}
