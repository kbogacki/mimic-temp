<?php
namespace Gstarczyk\Mimic\Stub\BehaviourBuilder;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\Stub\BehaviourBuilder\ConsecutiveInvocations;
use Gstarczyk\Mimic\Stub\BehaviourBuilder\SingleInvocation;
use Gstarczyk\Mimic\Stub\Stub;

class BehaviourBuilderSelector
{
    /** @var Stub */
    private $stub;

    public function __construct(Stub $stub)
    {
        $this->stub = $stub;
    }

    /**
     * @param string $methodName
     * @return SingleInvocation\ArgumentsChooser
     */
    public function invoke($methodName)
    {
        $builder = new SingleInvocation\BehaviourBuilder($this->stub, new ArgumentsMatcherFactory());
        $builder->invoke($methodName);

        return $builder;
    }

    /**
     * @param string $methodName
     * @return ConsecutiveInvocations\ReactionChooser
     */
    public function consecutiveInvoke($methodName)
    {
        $builder = new ConsecutiveInvocations\BehaviourBuilder($this->stub, new ArgumentsMatcherFactory());
        $builder->setMethodName($methodName);

        return $builder;
    }
}
