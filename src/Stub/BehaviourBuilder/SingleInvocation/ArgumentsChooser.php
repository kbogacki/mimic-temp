<?php
namespace Gstarczyk\Mimic\Stub\BehaviourBuilder\SingleInvocation;

interface ArgumentsChooser
{
    /**
     * @param $_ $arg1, $arg2, ...
     * @return ReactionChooser
     */
    public function with($_);

    /**
     * @return ReactionChooser
     */
    public function withAnyArguments();

    /**
     * @return ReactionChooser
     */
    public function withoutArguments();
}