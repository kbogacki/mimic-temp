<?php
namespace Gstarczyk\Mimic\MockInitiator;

class Context
{
    /** @var object */
    private $parentObject;

    /** @var Import[]  */
    private $imports = [];

    /** @var string */
    private $namespace;

    /**
     * @param object $parentObject
     * @param string $namespace
     * @param Import[] $imports
     */
    public function __construct($parentObject, $namespace, array $imports)
    {
        $this->parentObject = $parentObject;
        $this->setImports($imports);
        $this->namespace = $namespace;
    }

    /**
     * @param Import[] $imports
     */
    private function setImports(array $imports)
    {
        foreach ($imports as $import) {
            $this->appendImport($import);
        }
    }

    private function appendImport(Import $import)
    {
        $this->imports[] = $import;
    }

    /**
     * @return object
     */
    public function getParentObject()
    {
        return $this->parentObject;
    }

    /**
     * @return Import[]
     */
    public function getImports()
    {
        return $this->imports;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }
}