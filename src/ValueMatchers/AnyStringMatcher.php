<?php
namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class AnyStringMatcher implements ValueMatcher
{
    public function match($value)
    {
        return is_string($value);
    }
}