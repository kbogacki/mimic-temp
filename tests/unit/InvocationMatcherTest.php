<?php
namespace Gstarczyk\Mimic\UnitTest;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\InvocationSignature;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class InvocationMatcherTest extends \PHPUnit_Framework_TestCase
{
    public function testMatchReturnTrueOnlyForMatchingInvocations()
    {
        $argumentsMatcher = $this->createArgumentsMatcher(true);
        $invocation = new InvocationSignature('methodOne', []);
        $matcher = new InvocationMatcher('methodOne', $argumentsMatcher);
        $result = $matcher->match($invocation);

        $this->assertTrue($result);
    }

    public function testMatchReturnFalseWhenGivenMethodNameIsNotMatched()
    {
        $argumentsMatcher = $this->createArgumentsMatcher(true);
        $invocation = new InvocationSignature('methodOne', []);
        $matcher = new InvocationMatcher('methodTwo', $argumentsMatcher);
        $result = $matcher->match($invocation);

        $this->assertFalse($result);
    }

    public function testMatchReturnFalseWhenGivenArgumentsMatcherReturnFalse()
    {
        $argumentsMatcher = $this->createArgumentsMatcher(false);
        $invocation = new InvocationSignature('methodOne', []);
        $matcher = new InvocationMatcher('methodOne', $argumentsMatcher);
        $result = $matcher->match($invocation);

        $this->assertFalse($result);
    }

    /**
     * @param bool $result
     * @return ArgumentsMatcher | MockObject
     */
    private function createArgumentsMatcher($result)
    {
        $argumentsMatcher = $this->createMock(ArgumentsMatcher::class);
        $argumentsMatcher->method('match')->willReturn($result);

        return $argumentsMatcher;
    }
}
