<?php
namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\Mock\MethodArgument\ArrayArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\VariantArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use PHPUnit_Framework_Assert as Assert;
use ReflectionClass;
use ReflectionParameter;

class MethodArgumentFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var MethodArgumentFactory */
    private $factory;

    protected function setUp()
    {
        $this->factory = new MethodArgumentFactory();
    }

    public function testCreateDefinitionForObjectType()
    {
        $reflection = $this->createReflectionParameter([
            'getClass' => 'MyClass'
        ]);
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(ObjectArgumentDefinition::class, $result);
        Assert::assertFalse($result->isOptional());
    }

    public function testCreateDefinitionForArrayType()
    {
        $reflection = $this->createReflectionParameter([
            'isArray' => true
        ]);
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(ArrayArgumentDefinition::class, $result);
        Assert::assertFalse($result->isOptional());
    }

    public function testCreateDefinitionForMixedType()
    {
        $reflection = $this->createReflectionParameter([]);
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(VariantArgumentDefinition::class, $result);
        Assert::assertFalse($result->isOptional());
        Assert::assertFalse($result->isPassedByReference());
    }

    public function testCreateDefinitionForArrayTypeWithDefaultValue()
    {
        $reflection = $this->createReflectionParameter([
            'isArray' => true,
            'isDefaultValueAvailable' => true,
            'getDefaultValue' => ['item']
        ]);
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(ArrayArgumentDefinition::class, $result);
        Assert::assertEquals(['item'], $result->getDefaultValue());
    }

    public function testCreateDefinitionForMixedTypeWithDefaultValue()
    {
        $reflection = $this->createReflectionParameter([
            'isDefaultValueAvailable' => true,
            'getDefaultValue' => 'myDefaultValue'
        ]);
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(VariantArgumentDefinition::class, $result);
        Assert::assertEquals('myDefaultValue', $result->getDefaultValue());
    }

    public function testCreateDefinitionForOptionalParameter()
    {
        $reflection = $this->createReflectionParameter([
            'isOptional' => true,
        ]);
        $result = $this->factory->createArgument($reflection);

        Assert::assertTrue($result->isOptional());
    }

    public function testCreateDefinitionForParameterPassedByReference()
    {
        $reflection = $this->createReflectionParameter([
            'isPassedByReference' => true,
        ]);
        $result = $this->factory->createArgument($reflection);

        Assert::assertTrue($result->isPassedByReference());
    }

    private function createReflectionParameter(array $data)
    {
        $defaultData = [
            'getClass' => null,
            'isArray' => false,
            'isDefaultValueAvailable' => false,
            'getDefaultValue' => null,
            'isOptional' => false,
            'isPassedByReference' => false,
        ];
        if (isset($data['getClass'])) {
            $classReflection = $this->createMock(ReflectionClass::class);
            $classReflection->expects($this->any())
                ->method('getName')
                ->willReturn($data['getClass']);
            $data['getClass'] = $classReflection;
        }

        $mergedData = array_merge($defaultData, $data);
        $reflection = $this->createMock(ReflectionParameter::class);

        foreach ($mergedData as $name => $value) {
            $reflection->expects($this->any())
                ->method($name)
                ->willReturn($value);
        }

        return $reflection;
    }
}
