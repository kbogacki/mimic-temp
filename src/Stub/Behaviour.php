<?php
namespace Gstarczyk\Mimic\Stub;

use Gstarczyk\Mimic\InvocationMatcher;

class Behaviour
{
    /** @var Action */
    private $action;

    /** @var InvocationMatcher */
    private $matcher;

    /** @var int | null */
    private $invocationNumber;

    /**
     * @param Action $action
     * @param InvocationMatcher $matcher
     * @param int|null $invocationNumber
     */
    public function __construct(Action $action, InvocationMatcher $matcher, $invocationNumber = null)
    {
        $this->action = $action;
        $this->matcher = $matcher;
        $this->invocationNumber = $invocationNumber;
    }

    /**
     * @return Action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return InvocationMatcher
     */
    public function getMatcher()
    {
        return $this->matcher;
    }

    /**
     * @return bool
     */
    public function hasInvocationNumber()
    {
        return $this->invocationNumber !== null;
    }

    /**
     * @return int|null
     */
    public function getInvocationNumber()
    {
        return $this->invocationNumber;
    }
}
