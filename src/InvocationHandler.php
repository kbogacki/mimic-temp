<?php
namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\Stub\Stub;

class InvocationHandler
{
    /** @var InvocationRegistry */
    private $invocationRegistry;

    /** @var Stub */
    private $stub;

    public function __construct(InvocationRegistry $invocationRegistry, Stub $stub)
    {
        $this->invocationRegistry = $invocationRegistry;
        $this->stub = $stub;
    }

    /**
     * @param string $methodName
     * @param array $arguments
     * @return mixed
     */
    public function handleInvocation($methodName, array $arguments)
    {
        $invocationSignature = new InvocationSignature($methodName, $arguments);
        $this->invocationRegistry->registerInvocation($invocationSignature);
        $action = $this->stub->findAction($invocationSignature);

        return $action->perform($invocationSignature);
    }
}