<?php
namespace Gstarczyk\Mimic\MockInitiator;

use RuntimeException;

class FileReader
{
    /**
     * @param string $filePath
     * @throws RuntimeException when file not exist or cannot read file contents
     * @return string
     */
    public function getContents($filePath)
    {
        $this->validateFile($filePath);

        $contents = file_get_contents($filePath);
        if ($contents === false) {
            throw new RuntimeException(sprintf('Cannot read file content [filePath=%s]', $filePath));
        }

        return $contents;
    }

    private function validateFile($filePath)
    {
        if (!is_file($filePath) || !is_readable($filePath)) {
            throw new \RuntimeException(sprintf('File "%s" not exist', $filePath));
        }
    }

}