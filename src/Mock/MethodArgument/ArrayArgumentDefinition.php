<?php
namespace Gstarczyk\Mimic\Mock\MethodArgument;

class ArrayArgumentDefinition extends ArgumentDefinition implements DefaultValueAware
{
    /** @var  array */
    private $defaultValue;

    /**
     * @return array
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param array $defaultValue
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return string
     */
    public function toCode()
    {
        $result = sprintf('%sarray %s$%s',
            ($this->isNullAllowed() ? '?' : ''),
            ($this->isPassedByReference() ? '&' : ''),
            $this->getName()
        );

        if ($this->defaultValue !== null) {
            $result .= ' = ' . $this->createDefaultValueCode();
        } elseif ($this->isOptional()) {
            $result .= ' = null';
        }

        return $result;
    }

    private function createDefaultValueCode()
    {
        $items = [];
        foreach ($this->defaultValue as $item) {
            if (is_string($item)) {
                $items[] = sprintf('\'%s\'', $item);
            } else {
                $items[] = $item;
            }
        }
        return sprintf('[%s]', implode(', ', $items));
    }
}