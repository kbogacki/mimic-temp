<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Context;
use Gstarczyk\Mimic\MockInitiator\ContextFactory;
use Gstarczyk\Mimic\MockInitiator\ImportsExtractor;
use Gstarczyk\Mimic\UnitTest\Fixture\InitMocks\SimpleTestCase;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class ContextFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var ContextFactory */
    private $factory;

    /** @var ImportsExtractor | MockObject */
    private $importsExtractor;

    protected function setUp()
    {
        $this->importsExtractor = $this->createMock(ImportsExtractor::class);
        $this->factory = new ContextFactory(
            $this->importsExtractor
        );

        $this->importsExtractor->expects($this->any())
            ->method('getImports')
            ->willReturn([]);
    }

    public function testFactoryCreateContextWithParentObjectAndNamespace()
    {
        $parentObject = new SimpleTestCase();
        $result = $this->factory->createContext($parentObject, '');

        Assert::assertInstanceOf(Context::class, $result);
        Assert::assertSame($parentObject, $result->getParentObject());
        Assert::assertEquals('\Gstarczyk\Mimic\UnitTest\Fixture\InitMocks', $result->getNamespace());
    }

    public function testFactoryUsePathToParentObjectAndImportsExtractor()
    {
        $pathToParentObject = '/path';
        $this->importsExtractor->expects($this->once())
            ->method('getImports')
            ->with($pathToParentObject);

        $parentObject = new SimpleTestCase();
        $this->factory->createContext($parentObject, $pathToParentObject);
    }
}
