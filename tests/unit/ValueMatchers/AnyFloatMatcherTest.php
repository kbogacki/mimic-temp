<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\AnyFloatMatcher;

class AnyFloatMatcherTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @param int $value
     * @dataProvider matchingValueProvider
     */
    public function testMatcherReturnTrueWhenGivenValueIsFloat($value)
    {
        $matcher = new AnyFloatMatcher();
        $result = $matcher->match($value);

        $this->assertTrue($result);
    }

    /**
     * @param mixed $value
     * @dataProvider notMatchingValueProvider
     */
    public function testMatcherReturnFalseWhenGivenValueIsNotFloat($value)
    {
        $matcher = new AnyFloatMatcher();
        $result = $matcher->match($value);

        $this->assertFalse($result);
    }

    public function matchingValueProvider()
    {
        return [
            [1.234],
            [1.2e3],
            [7E-10]
        ];
    }

    public function notMatchingValueProvider()
    {
        return [
            [1],
            ['text'],
            [array()]
        ];
    }
}
