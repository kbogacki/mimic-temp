<?php
namespace Gstarczyk\Mimic\MockInitiator;

use ReflectionMethod;
use ReflectionParameter;
use RuntimeException;

class MethodArgumentsResolver
{
    /**
     * @param ReflectionMethod $constructor
     * @param ObjectProperty[] $dependencies
     *
     * @return array $arguments
     * @throws RuntimeException when arguments cannot be resolved
     */
    public function resolveArguments(ReflectionMethod $constructor, array $dependencies)
    {
        $arguments = [];
        foreach ($constructor->getParameters() as $parameter) {
            $this->validateParameter($parameter);
            $dependency = $this->getDependencyForParameter($dependencies, $parameter);
            $arguments[] = $dependency->getValue();
        }

        return $arguments;
    }

    private function getDependencyForParameter(array $dependencies, ReflectionParameter $parameter)
    {
        $className = '\\' . $parameter->getClass()->getName();
        $argumentName = $parameter->getName();
        $arguments = $this->filterByClassName($dependencies, $className);
        if (count($arguments) > 1) {
            $arguments = $this->filterByName($arguments, $argumentName);
        }

        if (count($arguments) > 1) {
            throw new RuntimeException(sprintf('Found more than one dependency for argument "%s"', $argumentName));
        } elseif (count($arguments) == 0) {
            throw new RuntimeException(sprintf('Cannot find dependency for argument "%s"', $argumentName));
        }

        return $arguments[0];
    }

    private function validateParameter(ReflectionParameter $parameter)
    {
        if (empty($parameter->getClass())) {
            throw new RuntimeException('Target objects with non object requirements are not supported');
        }
    }

    /**
     * @param ObjectProperty[] $dependencies
     * @param string $className
     *
     * @return ObjectProperty[]
     */
    private function filterByClassName(array $dependencies, $className)
    {
        $arguments = [];
        foreach ($dependencies as $dependency) {
            if ($dependency->getType() == $className) {
                $arguments[] = $dependency;
            }
        }

        return $arguments;
    }

    /**
     * @param ObjectProperty[] $dependencies
     * @param string $argumentName
     *
     * @return ObjectProperty[]
     */
    private function filterByName(array $dependencies, $argumentName)
    {
        $arguments = [];
        foreach ($dependencies as $dependency) {
            if ($dependency->getName() == $argumentName) {
                $arguments[] = $dependency;
            }
        }

        return $arguments;
    }
}
