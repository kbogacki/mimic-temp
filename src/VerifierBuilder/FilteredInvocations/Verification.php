<?php
namespace Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;

use Gstarczyk\Mimic\TimesVerifier;

interface Verification
{
    /**
     * @param TimesVerifier $timesVerifier
     * @return void
     * @throws \RuntimeException
     */
    public function wasCalled(TimesVerifier $timesVerifier);
}