<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

/**
 * test case with single mock
 */
class TestCaseWithMock
{
    /**
     * @var \Gstarczyk\Mimic\IntegrationTest\Fixture\Mock1
     * @mock
     */
    public $mockedObject;
}