<?php
namespace Gstarczyk\Mimic\UnitTest\Mock\MethodArgument;

use Gstarczyk\Mimic\Mock\MethodArgument\VariantArgumentDefinition;
use PHPUnit_Framework_Assert as Assert;

class VariantArgumentDefinitionTest extends \PHPUnit_Framework_TestCase
{
    public function testToCodeReturnArgumentCodeAsString()
    {
        $argument = new VariantArgumentDefinition('someArgument');
        Assert::assertEquals('$someArgument', $argument->toCode());
    }

    public function testToCodeReturnArgumentCodeWithNullDefaultValueForOptionalArgument()
    {
        $argument = new VariantArgumentDefinition('someArgument');
        $argument->makeOptional();
        Assert::assertEquals('$someArgument = null', $argument->toCode());
    }

    public function testToCodeReturnArgumentCodeWithDefaultValueIfSet()
    {
        $argument = new VariantArgumentDefinition('someArgument');
        $argument->setDefaultValue('defaultValue');
        Assert::assertEquals('$someArgument = \'defaultValue\'', $argument->toCode());
    }

    public function testDefaultValueHasHigherPriorityThatNullableFlag()
    {
        $argument = new VariantArgumentDefinition('someArgument');
        $argument->setDefaultValue(100);
        $argument->makeOptional();
        Assert::assertEquals('$someArgument = 100', $argument->toCode());
    }

    public function testToCodeWithPassedAsReference()
    {
        $argument = new VariantArgumentDefinition('someArgument');
        $argument->makePassedByReference();
        Assert::assertEquals('&$someArgument', $argument->toCode());
    }
}
