<?php
namespace Gstarczyk\Mimic\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatcher;

class AnyArguments implements ArgumentsMatcher
{
    public function match(array $arguments)
    {
        return true;
    }
}