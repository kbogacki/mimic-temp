<?php
namespace Gstarczyk\Mimic\UnitTest;

use Gstarczyk\Mimic\InvocationHandler;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Action;
use Gstarczyk\Mimic\Stub\Actions\DoNothing;
use Gstarczyk\Mimic\Stub\Stub;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class InvocationHandlerTest extends \PHPUnit_Framework_TestCase
{
    /** @var InvocationHandler */
    private $handler;

    /** @var InvocationRegistry | MockObject */
    private $invocationRegistry;

    /** @var Stub | MockObject */
    private $stub;

    protected function setUp()
    {
        $this->invocationRegistry = $this->createMock(InvocationRegistry::class);
        $this->stub = $this->createMock(Stub::class);
        $this->handler = new InvocationHandler(
            $this->invocationRegistry,
            $this->stub
        );
    }

    public function testHandlerRegisterIncomingInvocation()
    {
        $expectedInvocation = new InvocationSignature('someMethod', []);
        $this->invocationRegistry->expects($this->once())
            ->method('registerInvocation')
            ->with($expectedInvocation);

        $this->stub->expects($this->any())
            ->method('findAction')
            ->willReturn(new DoNothing());

        $this->handler->handleInvocation('someMethod', []);
    }

    public function testHandlerPerformActionForMatchingBehaviour()
    {
        $behaviour = $this->createMock(Action::class);
        $this->stub->expects($this->any())
            ->method('findAction')
            ->willReturn($behaviour);

        $expectedInvocation = new InvocationSignature('someMethod', []);
        $behaviour->expects($this->once())
            ->method('perform')
            ->with($expectedInvocation);

        $this->handler->handleInvocation('someMethod', []);
    }

    public function testHandlerReturnResultOfBehaviourAction()
    {
        $behaviour = $this->createMock(Action::class);
        $this->stub->expects($this->any())
            ->method('findAction')
            ->willReturn($behaviour);

        $expectedResult = 'someValue';
        $behaviour->expects($this->any())
            ->method('perform')
            ->willReturn($expectedResult);

        $result = $this->handler->handleInvocation('someMethod', []);

        Assert::assertEquals($expectedResult, $result);
    }

}
