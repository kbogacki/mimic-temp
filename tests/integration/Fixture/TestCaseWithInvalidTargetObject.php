<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

/**
 * test case with invalid target object
 */
class TestCaseWithInvalidTargetObject
{
    /**
     * @var \Gstarczyk\Mimic\IntegrationTest\Fixture\InvalidTargetObject
     * @injectMocks
     */
    public $object;
}