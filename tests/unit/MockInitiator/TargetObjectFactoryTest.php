<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\MethodArgumentsResolver;
use Gstarczyk\Mimic\MockInitiator\TargetObjectFactory;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use ReflectionClass;
use stdClass;

class TargetObjectFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var TargetObjectFactory */
    private $factory;

    /** @var ReflectionClass | MockObject */
    private $reflectionClass;

    /** @var MethodArgumentsResolver | MockObject */
    private $argumentsResolver;

    protected function setUp()
    {
        $this->argumentsResolver = $this->getMockBuilder(MethodArgumentsResolver::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->factory = new TargetObjectFactory($this->argumentsResolver);
        $this->reflectionClass = $this->getMockBuilder(ReflectionClass::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testsFactoryCreatesInstanceOfGivenClass()
    {
        $dependencies = [];
        $this->reflectionClass
            ->method('hasMethod')
            ->with('__construct')
            ->willReturn(true);
        $reflectionMethod = $this->getMockBuilder(\ReflectionMethod::class)->disableOriginalConstructor()->getMock();
        $this->reflectionClass
            ->method('getMethod')
            ->with('__construct')
            ->willReturn($reflectionMethod);

        $resolvedArguments = [
            new stdClass()
        ];
        $this->argumentsResolver
            ->method('resolveArguments')
            ->willReturn($resolvedArguments);

        $expected = new stdClass();
        $this->reflectionClass->expects($this->once())
            ->method('newInstanceArgs')
            ->with($resolvedArguments)
            ->willReturn($expected);

        $result = $this->factory->createTargetObject($this->reflectionClass, $dependencies);

        $this->assertSame($expected, $result);
    }

}
