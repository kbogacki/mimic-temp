<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Context;
use Gstarczyk\Mimic\MockInitiator\ContextFactory;
use Gstarczyk\Mimic\MockInitiator\ObjectProperty;
use Gstarczyk\Mimic\MockInitiator\ObjectPropertyFactory;
use Gstarczyk\Mimic\MockInitiator\PropertyExtractor;
use Gstarczyk\Mimic\UnitTest\Fixture\InitMocks\SimpleTestCase;
use InvalidArgumentException;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class PropertyExtractorTest extends \PHPUnit_Framework_TestCase
{
    /** @var PropertyExtractor */
    private $extractor;

    /** @var ObjectPropertyFactory | MockObject */
    private $propertyFactory;

    /** @var ContextFactory | MockObject */
    private $contextFactory;

    protected function setUp()
    {
        $this->propertyFactory = $this->createMock(ObjectPropertyFactory::class);
        $this->contextFactory = $this->createMock(ContextFactory::class);
        $this->extractor = new PropertyExtractor(
            $this->propertyFactory,
            $this->contextFactory
        );

        $this->propertyFactory
            ->expects($this->any())
            ->method('createObjectProperty')
            ->willReturn($this->createMock(ObjectProperty::class));

        $this->contextFactory->expects($this->any())
            ->method('createContext')
            ->willReturn(new Context(new \stdClass(), '', []));
    }

    public function testExtractReturnObjectPropertiesCollection()
    {
        $parentObject = new SimpleTestCase();
        $result = $this->extractor->extractProperties($parentObject);

        Assert::assertContainsOnlyInstancesOf(ObjectProperty::class, $result);
    }

    public function testExtractReturnAllPropertiesDefinedInParentObject()
    {
        $parentObject = new SimpleTestCase();
        $result = $this->extractor->extractProperties($parentObject);

        Assert::assertCount(2, $result);
    }

    public function testExtractThrowExceptionWhenNonObjectIsGiven()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->extractor->extractProperties(null);
    }

    public function testExtractorUsePropertyFactoryToCreateProperties()
    {
        $this->propertyFactory->expects($this->exactly(2))
            ->method('createObjectProperty');

        $parentObject = new SimpleTestCase();
        $this->extractor->extractProperties($parentObject);
    }
}
