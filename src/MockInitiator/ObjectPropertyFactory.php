<?php
namespace Gstarczyk\Mimic\MockInitiator;

use ReflectionProperty;

class ObjectPropertyFactory
{
    /** @var TypeResolver */
    private $typeResolver;

    public function __construct(TypeResolver $typeResolver)
    {
        $this->typeResolver = $typeResolver;
    }

    public function createObjectProperty(ReflectionProperty $property, Context $context)
    {
        $type = $this->typeResolver->resolveType(
            $property->getDocComment(),
            $context
        );
        $objectProperty = new ObjectProperty(
            $property,
            $context->getParentObject(),
            $type,
            $this->getMarker($property->getDocComment())
        );

        return $objectProperty;
    }

    private function getMarker($docComment)
    {
        $markers = [
            ObjectProperty::MARKER_MOCK,
            ObjectProperty::MARKER_TARGET
        ];

        foreach ($markers as $marker) {
            $pattern = '/\s' . $marker . '\s/';
            if (preg_match($pattern, $docComment)) {
                return $marker;
            }
        }

        return null;
    }
}