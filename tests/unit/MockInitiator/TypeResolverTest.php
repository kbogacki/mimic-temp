<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Context;
use Gstarczyk\Mimic\MockInitiator\Import;
use Gstarczyk\Mimic\MockInitiator\TypeResolver;
use PHPUnit_Framework_Assert as Assert;
use stdClass;

class TypeResolverTest extends \PHPUnit_Framework_TestCase
{
    /** @var TypeResolver */
    private $typeResolver;

    protected function setUp()
    {
        $this->typeResolver = new TypeResolver();
    }

    /**
     * @param string $docComment
     * @param string $expectedType
     * @dataProvider builtInTypesProvider
     */
    public function testResolverRecognizeBuiltInTypes($docComment, $expectedType)
    {
        $context = new Context(new stdClass(), '\\', []);
        $result = $this->typeResolver->resolveType($docComment, $context);

        Assert::assertEquals($result, $expectedType);
    }

    public function builtInTypesProvider()
    {
        return [
            ['/** @var string */', 'string'],
            ['/** @var int */', 'integer'],
            ['/** @var integer */', 'integer'],
            ['/** @var float */', 'float'],
            ['/** @var bool */', 'boolean'],
            ['/** @var boolean */', 'boolean'],
            ['/** @var array */', 'array'],
            ['/** @var mixed */', 'mixed'],
            ['/** @var resource */', 'resource'],
            ['/** @var callable */', 'callable'],
            ['/** @var callback */', 'callable'],
        ];
    }

    public function testResolverCanDetectFullClassNameAndUseItAsType()
    {
        $docComment = '/** @var \My\Name\Space\ClassOne */';
        $context = new Context(new stdClass(), '\Test\Name\Space', []);
        $result = $this->typeResolver->resolveType($docComment, $context);

        Assert::assertEquals('\My\Name\Space\ClassOne', $result);
    }

    public function testResolverReturnNullWhenDocCommentDoNotHaveTypeInfo()
    {
        $docComment = '/** @mock */';
        $context = new Context(new stdClass(), '\Test\Name\Space', []);
        $result = $this->typeResolver->resolveType($docComment, $context);

        Assert::assertNull($result);
    }

    public function testResolverUseTestCaseNamespaceWhenCannotFindImport()
    {
        $docComment = '/** @var ClassOne */';
        $context = new Context(new stdClass(), '\My\Name\Space', []);
        $result = $this->typeResolver->resolveType($docComment, $context);

        Assert::assertEquals('\My\Name\Space\ClassOne', $result);
    }

    public function testResolverSupportRelativeNamespaceUses()
    {
        $docComment = '/** @var Fixtures\ClassOne */';
        $context = new Context(new stdClass(), '\My\Name\Space', []);
        $result = $this->typeResolver->resolveType($docComment, $context);

        Assert::assertEquals('\My\Name\Space\Fixtures\ClassOne', $result);
    }

    public function testResolverCanMatchImportByShortClassName()
    {
        $imports = [
            new Import('\My\Name\Space', 'ClassTwo'),
            new Import('\My\Name\Space', 'ClassOne'),
        ];
        $docComment = '/** @var ClassOne */';
        $context = new Context(new stdClass(), '\My\Name\Space', $imports);
        $result = $this->typeResolver->resolveType($docComment, $context);

        Assert::assertSame('\My\Name\Space\ClassOne', $result);
    }

    public function testResolverCanMatchImportByAlias()
    {
        $imports = [
            new Import('\My\Name\Space', 'ClassTwo', 'ClassOne'),
            new Import('\My\Name\Space', 'ClassOne', 'ClassTwo'),
            new Import('\My\Name\Space', 'ClassThree'),
        ];
        $docComment = '/** @var ClassTwo */';
        $context = new Context(new stdClass(), '\My\Name\Space', $imports);
        $result = $this->typeResolver->resolveType($docComment, $context);

        Assert::assertSame('\My\Name\Space\ClassOne', $result);
    }

    /**
     * @param string $propertyType
     * @param Import[] $imports
     * @dataProvider relativeClassNameDataProvider
     */
    public function testResolverHandleImportsOfNamespace($propertyType, array $imports)
    {
        $docComment = sprintf('/** @var %s */', $propertyType);
        $context = new Context(new stdClass(), '\My\Name\Space', $imports);
        $result = $this->typeResolver->resolveType($docComment, $context);

        Assert::assertSame('\My\Name\Space\ClassTwo', $result);
    }

    public function relativeClassNameDataProvider()
    {
        return [
            [
                'Space\ClassTwo',
                [
                    new Import('\My\Name', 'Space'),
                    new Import('\My\Other\Name\Space', 'ClassOne'),
                    new Import('\My\Other\Name\Space', 'ClassThree'),
                ]
            ],
            [
                'Space\ClassTwo',
                [
                    new Import('\My\Name\Space', 'ClassTwo'),
                    new Import('\My\Name', 'Space'),
                    new Import('\My\Other\Name\Space', 'ClassThree'),
                ]
            ],
            [
                'Name\Space\ClassTwo',
                [
                    new Import('\My\Other\Name\Space', 'ClassTwo'),
                    new Import('\My', 'Name'),
                    new Import('\My\Other\Name\Space', 'ClassThree'),
                ]
            ],
        ];
    }
}
