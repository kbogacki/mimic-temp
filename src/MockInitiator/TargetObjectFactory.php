<?php
namespace Gstarczyk\Mimic\MockInitiator;

use ReflectionClass;

class TargetObjectFactory
{
    /** @var MethodArgumentsResolver */
    private $argumentsResolver;

    public function __construct(MethodArgumentsResolver $argumentsResolver)
    {
        $this->argumentsResolver = $argumentsResolver;
    }

    public function createTargetObject(ReflectionClass $reflectionClass, $dependencies)
    {
        $arguments = [];
        if ($reflectionClass->hasMethod('__construct')) {
            $constructor = $reflectionClass->getMethod('__construct');
            $arguments = $this->argumentsResolver->resolveArguments($constructor, $dependencies);
        }
        $object = $reflectionClass->newInstanceArgs($arguments);

        return $object;
    }
}
