<?php
namespace Gstarczyk\Mimic;

class UniqueIdGenerator
{
    /**
     * @return string
     */
    public function generateId()
    {
        return uniqid();
    }
}