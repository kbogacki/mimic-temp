<?php
namespace Gstarczyk\Mimic\UnitTest\Stub;

use Gstarczyk\Mimic\InvocationCounter;
use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Actions\DoNothing;
use Gstarczyk\Mimic\Stub\Behaviour;
use Gstarczyk\Mimic\Stub\Stub;
use PHPUnit_Framework_Assert as Assert;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class StubTest extends \PHPUnit_Framework_TestCase
{
    /** @var Stub */
    private $stub;

    /** @var InvocationCounter | MockObject */
    private $invocationCounter;

    protected function setUp()
    {
        $this->invocationCounter = $this->createMock(InvocationCounter::class);
        $this->stub = new Stub($this->invocationCounter);
    }

    public function testFindActionReturnFirstMatchingAction()
    {
        $this->registerNegativeBehaviour();
        $this->registerNegativeBehaviour();
        $behaviour = $this->registerPositiveBehaviour();
        $this->registerNegativeBehaviour();

        $expectedAction = $behaviour->getAction();

        $result = $this->stub->findAction(new InvocationSignature('methodOne', []));

        Assert::assertSame($expectedAction, $result);
    }

    public function testFindActionCheckInvocationCountWhenInvocationNumberWasRegistered()
    {
        $this->registerNegativeBehaviour();
        $this->registerNegativeBehaviour(2);
        $expectedAction = $this->registerPositiveBehaviour(2)->getAction();
        $this->registerNegativeBehaviour();
        $this->invocationCounter->method('invocationCount')->willReturn(2);

        $result = $this->stub->findAction(new InvocationSignature('methodOne', []));

        Assert::assertSame($expectedAction, $result);
    }

    public function testFindActionReturnDefaultActionWhenExpectedInvocationNoWasNotMet()
    {
        $expectedAction = new DoNothing();

        $this->registerNegativeBehaviour();
        $this->registerNegativeBehaviour();
        $this->registerPositiveBehaviour(4);
        $this->registerNegativeBehaviour();
        $this->invocationCounter->method('invocationCount')->willReturn(2);

        $result = $this->stub->findAction(new InvocationSignature('methodOne', []));

        Assert::assertNotSame($expectedAction, $result);
    }

    public function testFindActionReturnDoNotingActionWhenNoRegisteredActionsMatchGivenInvocation()
    {
        $action = $this->registerNegativeBehaviour()->getAction();

        $result = $this->stub->findAction(new InvocationSignature('methodOne', []));

        Assert::assertNotSame($action, $result);
        Assert::assertInstanceOf(DoNothing::class, $result);
    }

    private function registerPositiveBehaviour($invocationNo = null)
    {
        $behaviour = new Behaviour(
            new DoNothing(),
            $this->createPositiveMatcher(),
            $invocationNo
        );
        $this->stub->registerBehaviour($behaviour);

        return $behaviour;
    }

    private function registerNegativeBehaviour($invocationNo = null)
    {
        $behaviour = new Behaviour(
            new DoNothing(),
            $this->createNegativeMatcher(),
            $invocationNo
        );
        $this->stub->registerBehaviour($behaviour);

        return $behaviour;
    }

    /**
     * @return InvocationMatcher | MockObject
     */
    private function createPositiveMatcher()
    {
        $matcher = $this->createMock(InvocationMatcher::class);
        $matcher->method('match')->willReturn(true);

        return $matcher;
    }

    /**
     * @return InvocationMatcher | MockObject
     */
    private function createNegativeMatcher()
    {
        $matcher = $this->createMock(InvocationMatcher::class);
        $matcher->method('match')->willReturn(false);

        return $matcher;
    }
}
