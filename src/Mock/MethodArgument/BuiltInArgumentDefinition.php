<?php
namespace Gstarczyk\Mimic\Mock\MethodArgument;

class BuiltInArgumentDefinition extends VariantArgumentDefinition
{
    /** @var  string */
    private $type;

    /**
     * @param string $name
     * @param string $type
     */
    public function __construct($name, $type)
    {
        parent::__construct($name);
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function toCode()
    {
        $code = sprintf('%s%s %s',
            ($this->isNullAllowed() ? '?' : ''),
            $this->type,
            parent::toCode()
        );

        return $code;
    }
}