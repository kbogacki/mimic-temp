<?php
namespace Gstarczyk\Mimic\TimesVerifiers;

class Exactly extends AbstractVerifier
{
    public function verify($times)
    {
        if ($this->getExpectedTimes() != $times) {
            throw new TimesVerificationException(
                sprintf(
                    'Expected exactly %d, but %d was given.',
                    $this->getExpectedTimes(),
                    $times
                )
            );
        }
    }
}