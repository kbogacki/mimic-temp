<?php
namespace Gstarczyk\Mimic\Stub;

use Gstarczyk\Mimic\InvocationSignature;

interface Action
{
    /**
     * @param InvocationSignature $invocationSignature
     * @return mixed
     */
    public function perform(InvocationSignature $invocationSignature);
}