<?php

namespace Gstarczyk\Mimic\IntegrationTest\Mock;

use Gstarczyk\Mimic\IntegrationTest\Fixture\NotExistingClassAsMethodParams;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use PHPUnit_Framework_Assert as Assert;

class MethodArgumentFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var MethodArgumentFactory */
    private $factory;

    protected function setUp()
    {
        $this->factory = new MethodArgumentFactory();
    }

    /**
     * @param \ReflectionParameter $parameter
     * @param string $expectedClassName
     *
     * @dataProvider parameterProvider
     */
    public function testCreateObjectArgumentWithNotExistingClass(\ReflectionParameter $parameter, $expectedClassName)
    {
        $argument = $this->factory->createArgument($parameter);

        Assert::assertInstanceOf(ObjectArgumentDefinition::class, $argument);
        Assert::assertEquals($expectedClassName, $argument->getClassName());
    }

    public function parameterProvider()
    {
        $class = new \ReflectionClass(NotExistingClassAsMethodParams::class);
        $method1 = $class->getMethod('testMethodFullClassName');
        $method2 = $class->getMethod('testMethodShortClassName');

        return [
            'short class name' => [$method1->getParameters()[0], 'Not\Existing\Klass1'],
            'full class name' => [$method2->getParameters()[0], 'Not\Existing\Klass2'],
        ];
    }
}
