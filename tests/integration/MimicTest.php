<?php
namespace Gstarczyk\Mimic\IntegrationTest;

use Gstarczyk\Mimic\IntegrationTest\Fixture\ClassWithReturnType7;
use Gstarczyk\Mimic\IntegrationTest\Fixture\ClassWithReturnType71;
use Gstarczyk\Mimic\IntegrationTest\Fixture\InterfaceToMock;
use Gstarczyk\Mimic\IntegrationTest\Fixture\Mock1;
use Gstarczyk\Mimic\IntegrationTest\Fixture\Mock2;
use Gstarczyk\Mimic\IntegrationTest\Fixture\StaticMethods;
use Gstarczyk\Mimic\IntegrationTest\Fixture\TargetObject;
use Gstarczyk\Mimic\IntegrationTest\Fixture\TestCaseWithInvalidTargetObject;
use Gstarczyk\Mimic\IntegrationTest\Fixture\TestCaseWithMock;
use Gstarczyk\Mimic\IntegrationTest\Fixture\TestCaseWithMocksAndTargetObject;
use Gstarczyk\Mimic\IntegrationTest\Fixture\TestCaseWithMockWithShortClassName;
use Gstarczyk\Mimic\Match;
use Gstarczyk\Mimic\Mimic;
use Gstarczyk\Mimic\Times;
use PHPUnit_Framework_Assert as Assert;
use ReflectionMethod;
use RuntimeException;

class MimicTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatingMock()
    {
        $mock = Mimic::mock(Mock1::class);

        Assert::assertInstanceOf(Mock1::class, $mock);
    }

    public function testStubDefinition()
    {
        /** @var Mock1 $mock */
        $mock = Mimic::mock(Mock1::class);
        Mimic::when($mock)
            ->invoke('publicMethodOne')
            ->withAnyArguments()
            ->willReturn(300);

        Assert::assertEquals(300, $mock->publicMethodOne(200));
    }

    public function testStubWithArgumentsMatching()
    {
        /** @var Mock1 $mock */
        $mock = Mimic::mock(Mock1::class);
        Mimic::when($mock)
            ->invoke('publicMethodOne')
            ->with(200)
            ->willReturn(300);

        Mimic::when($mock)
            ->invoke('publicMethodOne')
            ->with(Match::anyString())
            ->willReturn(1000);

        Assert::assertEquals(300, $mock->publicMethodOne(200));
        Assert::assertEquals(1000, $mock->publicMethodOne('text'));
    }

    public function testStubWithCallbackAction()
    {
        /** @var Mock1 $mock */
        $mock = Mimic::mock(Mock1::class);
        Mimic::when($mock)
            ->invoke('publicMethodOne')
            ->withAnyArguments()
            ->willReturnCallbackResult(function ($arg) {
                return 'callback+' . $arg;
            });

        Assert::assertEquals('callback+200', $mock->publicMethodOne(200));
    }

    public function testStubConsecutiveInvocations()
    {
        /** @var Mock1 $mock */
        $mock = Mimic::mock(Mock1::class);
        Mimic::when($mock)
            ->consecutiveInvoke('publicMethodOne')
            ->willReturn('first')
            ->thenReturn('second')
            ->thenReturn('third')
        ;

        $result1 = $mock->publicMethodOne(1);
        $result2 = $mock->publicMethodOne(1);
        $result3 = $mock->publicMethodOne(1);

        Assert::assertEquals('first', $result1);
        Assert::assertEquals('second', $result2);
        Assert::assertEquals('third', $result3);

    }

    public function testVerifyInvocations()
    {
        /** @var Mock1 $mock */
        $mock = Mimic::mock(Mock1::class);

        $mock->publicMethodOne(1);
        $mock->publicMethodOne(2);
        $mock->publicMethodOne(3);

        Mimic::verify($mock)
            ->method('publicMethodOne')
            ->withAnyArguments()
            ->wasCalled(Times::exactly(3));
    }

    public function testDefinitionForPhp7()
    {
        if (version_compare(phpversion(), '7.0.0', '<')) {
            $this->markTestSkipped('Your PHP version is too low (require PHP >= 7.0');
        }
        $mock = Mimic::mock(ClassWithReturnType7::class);

        $method = new ReflectionMethod($mock, 'methodWithPhp7Return');
        $this->assertTrue($method->hasReturnType());
        $this->assertTrue($method->getReturnType()->isBuiltin());
        $this->assertEquals('string', $method->getReturnType()->getName());

        $methodWithParam = new ReflectionMethod($mock, 'methodWithPhp7Parametric');
        $allMethodParams = $methodWithParam->getParameters();
        /** @var \ReflectionParameter $methodParam */
        $methodParam = reset($allMethodParams);
        $this->assertTrue($methodParam->hasType());
        $this->assertEquals('string', $methodParam->getType()->getName());

        $methodWithObjectReturn = new ReflectionMethod($mock, 'methodWithPhp7ObjectReturn');
        $this->assertTrue($methodWithObjectReturn->hasReturnType());
        $this->assertTrue($methodWithObjectReturn->getReturnType()->allowsNull());
        $this->assertFalse($methodWithObjectReturn->getReturnType()->isBuiltin());
        $this->assertEquals('Gstarczyk\Mimic\IntegrationTest\Fixture\Mock2', $methodWithObjectReturn->getReturnType()->getName());
    }
    public function testDefinitionForPhp71()
    {
        if (version_compare(phpversion(), '7.1.0', '<')) {
            $this->markTestSkipped('Your PHP version is too low (require PHP >= 7.0');
        }
        $mock = Mimic::mock(ClassWithReturnType71::class);

        $method = new ReflectionMethod($mock, 'methodWithPhp71NullableReturn');
        $this->assertTrue($method->hasReturnType());
        $this->assertEquals('string', $method->getReturnType()->getName());
        $this->assertTrue($method->getReturnType()->allowsNull());

        $methodVoid = new ReflectionMethod($mock, 'methodWithPhp71VoidReturn');
        $this->assertTrue($methodVoid->hasReturnType());
        $this->assertEquals('void', $methodVoid->getReturnType()->getName());

        $methodWithParam = new ReflectionMethod($mock, 'methodWithPhp71NullableParametric');
        $allMethodParams = $methodWithParam->getParameters();
        /** @var \ReflectionParameter $methodParam */
        $methodParam = reset($allMethodParams);
        $this->assertTrue($methodParam->hasType());
        $this->assertTrue($methodParam->allowsNull());
        $this->assertEquals('string', $methodParam->getType()->getName());
    }

    public function testVerifyInvocationsWithArgumentsMatching()
    {
        /** @var Mock1 $mock */
        $mock = Mimic::mock(Mock1::class);

        $mock->publicMethodOne(1);
        $mock->publicMethodOne(2);
        $mock->publicMethodOne(3);
        $mock->publicMethodOne('4');

        Mimic::verify($mock)
            ->method('publicMethodOne')
            ->with(Match::anyInteger())
            ->wasCalled(Times::exactly(3));

        Mimic::verify($mock)
            ->method('publicMethodOne')
            ->with(Match::equal(1))
            ->wasCalled(Times::exactly(1));

        Mimic::verify($mock)
            ->method('publicMethodOne')
            ->with(1)
            ->wasCalled(Times::exactly(1));

        Mimic::verify($mock)
            ->method('publicMethodOne')
            ->with(Match::anyString())
            ->wasCalled(Times::exactly(1));

    }

    public function testMockInitialization()
    {
        $testCase = new TestCaseWithMock();
        Mimic::initMocks($testCase);

        Assert::assertInstanceOf(Mock1::class, $testCase->mockedObject);
    }

    public function testMockInitializationWithPassedPathToTestCaseFile()
    {
        $testCase = new TestCaseWithMockWithShortClassName();
        Mimic::initMocks($testCase, __DIR__.'/Fixture/TestCaseWithMockWithShortClassName.php');

        Assert::assertInstanceOf(Mock1::class, $testCase->mockedObject);
    }

    public function testMockInjection()
    {
        $testCase = new TestCaseWithMocksAndTargetObject();
        Mimic::initMocks($testCase);

        Assert::assertInstanceOf(InterfaceToMock::class, $testCase->mock1);
        Assert::assertInstanceOf(Mock2::class, $testCase->mock2);
        Assert::assertInstanceOf(Mock1::class, $testCase->mock3);
        Assert::assertInstanceOf(TargetObject::class, $testCase->object);
        Assert::assertSame($testCase->mock1, $testCase->object->mock1);
        Assert::assertSame($testCase->mock2, $testCase->object->mock2);
        Assert::assertSame($testCase->mock3, $testCase->object->mock3);
    }

    public function testMockInjectionThrowExceptionWhenTargetObjectHasNonObjectRequirements()
    {
        $this->expectException(RuntimeException::class);

        $testCase = new TestCaseWithInvalidTargetObject();
        Mimic::initMocks($testCase);
    }

    public function testStaticMethodMocking()
    {
        Mimic::mock(StaticMethods::class);
    }
}
