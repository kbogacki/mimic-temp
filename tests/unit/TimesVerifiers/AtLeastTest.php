<?php
namespace Gstarczyk\Mimic\UnitTest\TimesVerifiers;

use Gstarczyk\Mimic\TimesVerifiers\AtLeast;
use Gstarczyk\Mimic\TimesVerifiers\TimesVerificationException;

class AtLeastTest extends \PHPUnit_Framework_TestCase
{
    public function testMatcherThrowExceptionWhenExpectationDoesNotMet()
    {
        $this->expectException(TimesVerificationException::class);
        $this->expectExceptionMessage('Expected at least 1, but 0 was given.');

        $verifier = new AtLeast(1);
        $verifier->verify(0);
    }
}
