<?php
namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\ValueMatchers\AnyFloatMatcher;
use Gstarczyk\Mimic\ValueMatchers\AnyIntegerMatcher;
use Gstarczyk\Mimic\ValueMatchers\AnyObjectMatcher;
use Gstarczyk\Mimic\ValueMatchers\AnyStringMatcher;
use Gstarczyk\Mimic\ValueMatchers\AnyTraversableMatcher;
use Gstarczyk\Mimic\ValueMatchers\EqualMatcher;
use Gstarczyk\Mimic\ValueMatchers\SameMatcher;
use Gstarczyk\Mimic\ValueMatchers\StringEndsWith;
use Gstarczyk\Mimic\ValueMatchers\StringStartsWith;

class Match
{
    static public function equal($value)
    {
        return new EqualMatcher($value);
    }

    static public function same($value)
    {
        return new SameMatcher($value);
    }

    static public function anyString()
    {
        return new AnyStringMatcher();
    }

    static public function anyInteger()
    {
        return new AnyIntegerMatcher();
    }

    static public function anyFloat()
    {
        return new AnyFloatMatcher();
    }

    static public function anyTraversable()
    {
        return new AnyTraversableMatcher();
    }

    static public function anyObject($className = null)
    {
        return new AnyObjectMatcher($className);
    }

    static public function stringStartsWith($prefix)
    {
        return new StringStartsWith($prefix);
    }

    static public function stringEndsWith($prefix)
    {
        return new StringEndsWith($prefix);
    }
}
