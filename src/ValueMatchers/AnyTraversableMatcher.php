<?php
namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;
use Traversable;

class AnyTraversableMatcher implements ValueMatcher
{
    public function match($value)
    {
        return is_array($value) || ($value instanceOf Traversable);
    }
}
