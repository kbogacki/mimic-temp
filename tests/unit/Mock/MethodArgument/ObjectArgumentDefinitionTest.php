<?php
namespace Gstarczyk\Mimic\UnitTest\Mock\MethodArgument;

use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use PHPUnit_Framework_Assert as Assert;

class ObjectArgumentDefinitionTest extends \PHPUnit_Framework_TestCase
{
    public function testToCodeReturnArgumentCodeAsString()
    {
        $argument = new ObjectArgumentDefinition('someArgument', \DateTime::class);
        Assert::assertEquals('\DateTime $someArgument', $argument->toCode());
    }

    public function testToCodeReturnArgumentCodeWithNullDefaultValueForOptionalArgument()
    {
        $argument = new ObjectArgumentDefinition('someArgument', \DateTime::class);
        $argument->makeOptional();
        Assert::assertEquals('\DateTime $someArgument = null', $argument->toCode());
    }

    public function testToCodeWithPassedAsReference()
    {
        $argument = new ObjectArgumentDefinition('someArgument', \DateTime::class);
        $argument->makePassedByReference();
        Assert::assertEquals('\DateTime &$someArgument', $argument->toCode());
    }
}
