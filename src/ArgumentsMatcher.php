<?php
namespace Gstarczyk\Mimic;

interface ArgumentsMatcher
{
    /**
     * @param array $arguments
     * @return bool
     */
    public function match(array $arguments);
}