<?php
namespace Gstarczyk\Mimic;

class InvocationMatcher
{
    /** @var string */
    private $methodName;

    /** @var ArgumentsMatcher */
    private $argumentsMatcher;

    /**
     * @param string $methodName
     * @param ArgumentsMatcher $argumentsMatcher
     */
    public function __construct($methodName, ArgumentsMatcher $argumentsMatcher)
    {
        $this->methodName = $methodName;
        $this->argumentsMatcher = $argumentsMatcher;
    }

    /**
     * @param InvocationSignature $invocationSignature
     * @return bool
     */
    public function match(InvocationSignature $invocationSignature)
    {
        $match = false;
        if ($this->methodName == $invocationSignature->getMethodName()) {
            $match = $this->argumentsMatcher->match($invocationSignature->getArguments());
        }

        return $match;
    }
}