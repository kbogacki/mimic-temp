<?php
namespace Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;

interface ConsecutiveInvocationArguments
{
    /**
     * @param $_
     * @return self
     */
    public function thenWith($_);

    /**
     * @return self
     */
    public function thenWithAnyArguments();

    /**
     * @return self
     */
    public function thenWithoutArguments();
}