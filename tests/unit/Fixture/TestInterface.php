<?php
namespace Gstarczyk\Mimic\UnitTest\Fixture;

interface TestInterface
{
    function someMethod();
    public function someOtherMethod($arg1, array $arg2);
}