<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\AnyStringMatcher;

class AnyStringMatcherTest extends \PHPUnit_Framework_TestCase
{
    public function testMatcherReturnTrueWhenGivenValueIsString()
    {
        $matcher = new AnyStringMatcher();
        $value = 'some text';
        $result = $matcher->match($value);

        $this->assertTrue($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsNotString()
    {
        $matcher = new AnyStringMatcher();
        $value = 100;
        $result = $matcher->match($value);

        $this->assertFalse($result);
    }
}
