<?php
namespace Gstarczyk\Mimic\IntegrationTest\Mock;

use Gstarczyk\Mimic\InvocationHandler;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\Mock\ClassDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use Gstarczyk\Mimic\Mock\MockFactory;
use Gstarczyk\Mimic\Stub\Stub;
use Gstarczyk\Mimic\UniqueIdGenerator;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassOne;

class MockFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var MockFactory */
    private $factory;

    protected function setUp()
    {
        $defFactory = new ClassDefinitionFactory(
            new UniqueIdGenerator(),
            new MethodArgumentFactory()
        );
        $this->factory = new MockFactory($defFactory);
    }

    public function testFactoryCreatesMockClassExtendingGivenClass()
    {
        $invocationHandler = $this->createInvocationHandler();
        $result = $this->factory->createMock(ClassOne::class, $invocationHandler);

        $this->assertInstanceOf(ClassOne::class, $result);
    }

    public function testAllMockedMethodsReturnNull()
    {
        $invocationHandler = $this->createInvocationHandler();

        /** @var ClassOne $result */
        $result = $this->factory->createMock(ClassOne::class, $invocationHandler);

        $this->assertNull($result->publicMethodOne('assad'));
        $this->assertNull($result->publicMethodTwo(new \DateTime(), []));
        $this->assertNull($result->publicMethodThree($result));
        $this->assertNull($result->publicMethodFour('str', 100));
    }

    /**
     * @return InvocationHandler
     */
    private function createInvocationHandler()
    {
        $invocationRegistry = new InvocationRegistry();
        $invocationHandler = new InvocationHandler(
            $invocationRegistry, new Stub($invocationRegistry)
        );

        return $invocationHandler;
    }

}
