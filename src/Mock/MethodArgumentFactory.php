<?php
namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\Mock\MethodArgument\ArrayArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\BuiltInArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\DefaultValueAware;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\VariantArgumentDefinition;
use ReflectionException;
use ReflectionParameter;

class MethodArgumentFactory
{
    public function createArgument(ReflectionParameter $parameter)
    {
        if ($parameter->isArray()) {
            $argument = new ArrayArgumentDefinition($parameter->getName());
        } elseif ($className = $this->getClassName($parameter)) {
            $argument = new ObjectArgumentDefinition($parameter->getName(), $className);
        } elseif (method_exists($parameter, 'getType') && $parameter->getType() !== null) {
            $argument = new BuiltInArgumentDefinition($parameter->getName(), $parameter->getType());
        } else {
            $argument = new VariantArgumentDefinition($parameter->getName());
        }

        if ($parameter->isDefaultValueAvailable() && $argument instanceof DefaultValueAware) {
            $argument->setDefaultValue($parameter->getDefaultValue());
        }

        if ($parameter->isOptional()) {
            $argument->makeOptional();
        }
        if ($parameter->isPassedByReference()) {
            $argument->makePassedByReference();
        }
        if (method_exists($parameter, 'allowsNull') && $parameter->allowsNull() && !$parameter->isOptional()) {
            $argument->makeNullAllowed();
        }

        return $argument;
    }

    private function getClassName(ReflectionParameter $parameter)
    {
        try {
            $class = $parameter->getClass();
            $className = $class ? $class->getName() : false;
        } catch (ReflectionException $exception) {
            $text = $parameter->__toString();
            $matches = [];
            preg_match('/\[\s\<\w+?>\s([\\\\\w]+)/s', $text, $matches);

            $className = isset($matches[1]) ? $matches[1] : false;
        }

        return $className;
    }
}
