<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\MethodArgumentsResolver;
use Gstarczyk\Mimic\MockInitiator\ObjectProperty;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;
use stdClass;

class MethodArgumentsResolverTest extends \PHPUnit_Framework_TestCase
{
    /** @var MethodArgumentsResolver */
    private $resolver;

    protected function setUp()
    {
        $this->resolver = new MethodArgumentsResolver();
    }

    public function testResolverThrowExceptionWhenAnyArgumentIsNotObject()
    {
        $this->expectException(\RuntimeException::class);

        $dependencies = [
            $this->createObjectPropertyMock('property1', '\MyNameSpace\MyClass'),
            $this->createObjectPropertyMock('property2', '\MyNameSpace\MyOtherClass'),
        ];
        $constructor = $this->createReflectionMethodMock([
            $this->createReflectionParameterMock('myObject', null)
        ]);

        $this->resolver->resolveArguments($constructor, $dependencies);
    }

    public function testResolverThrowExceptionWhenCannotFindDependencyForAnyArgument()
    {
        $this->expectException(\RuntimeException::class);

        $dependencies = [
            $this->createObjectPropertyMock('property1', '\MyNameSpace\MyClass'),
            $this->createObjectPropertyMock('property2', '\MyNameSpace\MyOtherClass'),
        ];
        $constructor = $this->createReflectionMethodMock([
            $this->createReflectionParameterMock('myObject', 'MyNameSpace\MyClass2')
        ]);

        $this->resolver->resolveArguments($constructor, $dependencies);
    }

    public function testResolverThrowExceptionWhenFoundMoreThanOneDependencyForArgument()
    {
        $this->expectException(\RuntimeException::class);

        $dependencies = [
            $this->createObjectPropertyMock('property1', '\MyNameSpace\MyClass'),
            $this->createObjectPropertyMock('property1', '\MyNameSpace\MyClass'),
        ];
        $constructor = $this->createReflectionMethodMock([
            $this->createReflectionParameterMock('myObject', 'MyNameSpace\MyClass')
        ]);

        $this->resolver->resolveArguments($constructor, $dependencies);
    }

    public function testResolverMatchArgumentsAndDependenciesByClassName()
    {
        $mock1 = new stdClass();
        $mock2 = new stdClass();
        $dependencies = [
            $this->createObjectPropertyMock('property1', '\MyNameSpace\MyClass', $mock1),
            $this->createObjectPropertyMock('property2', '\MyNameSpace\MyOtherClass', $mock2),
        ];
        $constructor = $this->createReflectionMethodMock([
            $this->createReflectionParameterMock('myObject', 'MyNameSpace\MyClass')
        ]);

        $result = $this->resolver->resolveArguments($constructor, $dependencies);

        $this->assertCount(1, $result);
        $this->assertSame($mock1, $result[0]);
    }

    public function testResolverMatchArgumentsAndDependenciesByNameWhenClassNameMatchingIsInsufficient()
    {
        $mock1 = new stdClass();
        $mock2 = new stdClass();
        $dependencies = [
            $this->createObjectPropertyMock('property1', '\MyNameSpace\MyClass', $mock1),
            $this->createObjectPropertyMock('property2', '\MyNameSpace\MyClass', $mock2),
        ];
        $constructor = $this->createReflectionMethodMock([
            $this->createReflectionParameterMock('property2', 'MyNameSpace\MyClass')
        ]);

        $result = $this->resolver->resolveArguments($constructor, $dependencies);

        $this->assertCount(1, $result);
        $this->assertSame($mock2, $result[0]);
    }

    /**
     * @param string $propertyName
     * @param string $className
     * @param object $value
     * @return ObjectProperty|MockObject
     */
    private function createObjectPropertyMock($propertyName, $className, $value = null)
    {
        $property = $this->getMockBuilder(ObjectProperty::class)
            ->disableOriginalConstructor()
            ->getMock();

        $property->expects($this->any())
            ->method('getType')
            ->willReturn($className);

        $property->expects($this->any())
            ->method('getName')
            ->willReturn($propertyName);

        $property->expects($this->any())
            ->method('getValue')
            ->willReturn($value);

        return $property;
    }

    /**
     * @param $parameterName
     * @param $parameterClassName
     *
     * @return ReflectionParameter | MockObject
     */
    private function createReflectionParameterMock($parameterName, $parameterClassName)
    {
        if ($parameterClassName !== null) {
            $class = $this->getMockBuilder(ReflectionClass::class)
                ->disableOriginalConstructor()
                ->getMock();
            $class->expects($this->any())
                ->method('getName')
                ->willReturn($parameterClassName);
        } else {
            $class = null;
        }

        $parameter = $this->getMockBuilder(ReflectionParameter::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parameter->expects($this->any())
            ->method('getClass')
            ->willReturn($class);
        $parameter->expects($this->any())
            ->method('getName')
            ->willReturn($parameterName);

        return $parameter;
    }

    /**
     * @param array $parameters
     *
     * @return ReflectionMethod | MockObject
     */
    private function createReflectionMethodMock(array $parameters)
    {
        $method = $this->getMockBuilder(ReflectionMethod::class)
            ->disableOriginalConstructor()
            ->getMock();
        $method->expects($this->any())
            ->method('getParameters')
            ->willReturn($parameters);

        return $method;
    }
}
