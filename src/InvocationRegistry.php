<?php
namespace Gstarczyk\Mimic;

class InvocationRegistry implements InvocationCounter
{
    /** @var InvocationSignature[] */
    private $invocations = [];

    public function registerInvocation(InvocationSignature $invocationSignature)
    {
        $this->invocations[] = $invocationSignature;
    }

    /**
     * @param string $methodName
     * @return InvocationSignature[]
     */
    public function getMethodInvocations($methodName)
    {
        $result = [];
        foreach ($this->invocations as $invocation) {
            if ($invocation->getMethodName() == $methodName) {
                $result[] = $invocation;
            }
        }

        return $result;
    }

    public function invocationCount(InvocationMatcher $invocationMatcher)
    {
        $count = 0;
        foreach ($this->invocations as $invocation) {
            if ($invocationMatcher->match($invocation)) {
                ++$count;
            }
        }

        return $count;
    }
}