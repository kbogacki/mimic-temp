<?php
namespace Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\TimesVerifier;
use Gstarczyk\Mimic\TimesVerifiers\TimesVerificationException;

class VerifierBuilder implements MethodChooser, ArgumentsChooser, Verification
{
    /** @var InvocationRegistry */
    private $invocationRegistry;

    /** @var string */
    private $methodName;

    /** @var ArgumentsMatcher */
    private $argumentsMatcher;
    
    /** @var ArgumentsMatcherFactory */
    private $matchingArgumentsFactory;

    public function __construct(InvocationRegistry $invocationRegistry, ArgumentsMatcherFactory $matchingArgumentsFactory)
    {
        $this->invocationRegistry = $invocationRegistry;
        $this->matchingArgumentsFactory = $matchingArgumentsFactory;
    }

    /**
     * @param string $methodName
     * @return $this
     */
    public function method($methodName)
    {
        $this->methodName = $methodName;

        return $this;
    }

    public function with($_)
    {
        $this->argumentsMatcher = $this->matchingArgumentsFactory->createMatcher(func_get_args());

        return $this;
    }

    public function withAnyArguments()
    {
        $this->argumentsMatcher = $this->matchingArgumentsFactory->createMatcher(null);

        return $this;
    }

    public function withoutArguments()
    {
        $this->argumentsMatcher = $this->matchingArgumentsFactory->createMatcher([]);

        return $this;
    }

    public function wasCalled(TimesVerifier $timesVerifier)
    {
        try {
            $invocationCount = $this->invocationRegistry->invocationCount(
                new InvocationMatcher($this->methodName, $this->argumentsMatcher)
            );
            $timesVerifier->verify($invocationCount);
        } catch (TimesVerificationException $e) {
            throw new TimesVerificationException(
                sprintf(
                    'Unmet expected invocation count of method "%s". %s',
                    $this->methodName,
                    $e->getMessage()
                )
            );
        }
    }
}