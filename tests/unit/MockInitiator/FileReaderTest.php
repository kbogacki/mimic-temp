<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\FileReader;
use RuntimeException;

class FileReaderTest extends \PHPUnit_Framework_TestCase
{
    const FILE_CONTENTS = 'some file content';

    /** @var FileReader */
    private $reader;

    protected function setUp()
    {
        $this->reader = new FileReader();
    }

    public function testReaderThrowExceptionWhenCannotReadFileContents()
    {
        $this->expectException(RuntimeException::class);

        $this->reader->getContents('\path\to\file.ext');
    }
}
