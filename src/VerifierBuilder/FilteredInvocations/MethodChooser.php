<?php
namespace Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;

interface MethodChooser
{
    /**
     * @param string $methodName
     * @return ArgumentsChooser
     */
    public function method($methodName);
}