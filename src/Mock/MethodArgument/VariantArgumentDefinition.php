<?php
namespace Gstarczyk\Mimic\Mock\MethodArgument;

class VariantArgumentDefinition extends ArgumentDefinition implements DefaultValueAware
{
    /** @var  string */
    private $defaultValue;
    
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return string
     */
    public function toCode()
    {
        $result = sprintf('%s$%s',
            ($this->isPassedByReference() ? '&' : ''),
            $this->getName()
        );
        if (is_string($this->defaultValue)) {
            $result .= sprintf(' = \'%s\'', $this->defaultValue);
        } elseif ($this->defaultValue != null) {
            $result .= ' = ' . $this->defaultValue;
        } elseif ($this->isOptional()) {
            $result .= ' = null';
        }

        return $result;
    }
}