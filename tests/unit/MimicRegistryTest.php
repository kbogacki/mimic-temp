<?php
namespace Gstarczyk\Mimic\UnitTest;

use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\MimicRegistry;
use Gstarczyk\Mimic\Mock\MockFactory;
use Gstarczyk\Mimic\Stub\Stub;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class MimicRegistryTest extends \PHPUnit_Framework_TestCase
{
    /** @var MimicRegistry */
    private $mimicRegistry;

    /** @var MockFactory | MockObject */
    private $mockFactory;

    public function setUp()
    {
        $this->mockFactory = $this->getMockBuilder(MockFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mimicRegistry = new MimicRegistry($this->mockFactory);
    }

    public function testRegistryThrowExceptionWhenNoStubIsFound()
    {
        $this->expectException(\RuntimeException::class);
        $this->mimicRegistry->getStub(new \stdClass());
    }

    public function testRegistryThrowExceptionWhenNoInvocationRegistryIsFound()
    {
        $this->expectException(\RuntimeException::class);
        $this->mimicRegistry->getInvocationRegistry(new \stdClass());
    }

    public function testGetMockCreatesNewMockAndRegisterStubAndInvocationRegistry()
    {
        $this->mockFactory->method('createMock')
            ->willReturn(new \stdClass());

        $mock = $this->mimicRegistry->getMock('stdClass');

        $stub = $this->mimicRegistry->getStub($mock);
        $invocationRegistry = $this->mimicRegistry->getInvocationRegistry($mock);

        $this->assertInstanceOf(Stub::class, $stub);
        $this->assertInstanceOf(InvocationRegistry::class, $invocationRegistry);
    }
}
