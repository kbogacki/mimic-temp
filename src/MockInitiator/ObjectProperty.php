<?php
namespace Gstarczyk\Mimic\MockInitiator;

use ReflectionProperty;

class ObjectProperty
{
    const MARKER_MOCK = '@mock';
    const MARKER_TARGET = '@injectMocks';

    /** @var ReflectionProperty */
    private $reflectionProperty;

    /** @var object */
    private $parentObject;

    /** @var string */
    private $type;

    /** @var string */
    private $marker;

    /**
     * @param ReflectionProperty $reflectionProperty
     * @param object $parentObject
     * @param string | null $type
     * @param string $marker
     */
    public function __construct(ReflectionProperty $reflectionProperty, $parentObject, $type, $marker)
    {
        $this->reflectionProperty = $reflectionProperty;
        $this->parentObject = $parentObject;
        $this->type = $type;
        $this->marker = $marker;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->reflectionProperty->getName();
    }

    /**
     * @return string | null
     */
    public function getType()
    {
        return $this->type;
    }

    public function getValue()
    {
        return $this->reflectionProperty->getValue($this->parentObject);
    }

    public function setValue($value)
    {
        $this->reflectionProperty->setAccessible(true);
        $this->reflectionProperty->setValue($this->parentObject, $value);
    }

    /**
     * @return bool
     */
    public function isMarkedAsMock()
    {
        return $this->marker === self::MARKER_MOCK;
    }

    /**
     * @return bool
     */
    public function isMarkedAsMocksTarget()
    {
        return $this->marker === self::MARKER_TARGET;
    }
}
