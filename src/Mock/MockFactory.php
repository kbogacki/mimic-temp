<?php
namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\InvocationHandler;

class MockFactory
{
    /** @var  ClassDefinitionFactory */
    private $definitionFactory;

    public function __construct(ClassDefinitionFactory $definitionFactory)
    {
        $this->definitionFactory = $definitionFactory;
    }

    /**
     * @param string $className
     * @param InvocationHandler $invocationHandler
     * @return object
     */
    public function createMock($className, InvocationHandler $invocationHandler)
    {
        $definition = $this->definitionFactory->createClassDefinition($className);
        $this->loadClass($definition);
        $fullClassName = $definition->getClassName();

        return new $fullClassName($invocationHandler);
    }

    private function loadClass(ClassDefinition $definition)
    {
        if (!class_exists($definition->getClassName(), false)) {
            eval($definition->toCode());
        }
    }
}