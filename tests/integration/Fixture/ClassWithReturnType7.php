<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;


class ClassWithReturnType7
{
    public function methodWithPhp7Return($arg): string
    {
        return $arg;
    }

    public function methodWithPhp7Parametric(string $arg): string
    {
        return $arg;
    }

    public function methodWithPhp7ObjectReturn(): ?Mock2
    {
        return null;
    }
}
